<?php

// sentence.php

return [
  'select' => 'Select',
  'companies' => 'Companies',
  'employees' => 'Employees',
  'employee' => 'Employee',
  'phone' => 'Phone',
  'first' => 'First',
  'last' => 'Last',
  'logo' => 'Logo',
  'name' => 'Name',
  'email' => 'Email',
  'actions' => 'Actions',
  'dashboard' => 'Dashboard',
  'welcome-message' => 'You are logged in!',
  'hi' => 'Hi',
  'logout' => 'Logout',
  'language' => 'Language',
  'listing' => 'Listing',
  'add' => 'Add',
  'edit' => 'Edit',
  'view' => 'View',
  'delete' => 'Delete',
  'update' => 'Update',
  'create' => 'Create',
  'company' => 'Company',
  'website' => 'Website',
];