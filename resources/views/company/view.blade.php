@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{ trans('sentence.view')." ".trans('sentence.company') }}
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group">
                    <label>{{ trans('sentence.name') }}:</label>
                    <input class="form-control" type="text" name="name" value="{{ $company->name }}" readonly>
                </div>
                <div class="form-group">
                    <label>{{ trans('sentence.email') }}:</label>
                    <input class="form-control" type="email" name="email" value="{{ $company->email }}" readonly>
                </div>
                <div class="form-group">
                    <label>{{ trans('sentence.logo') }}:</label>
                    <div class="thumb-container">
                        <img src="{{ asset('public/storage/'.$company->logo) }}" class="img-fluid actualthumb" alt="">
                    </div>
                </div>
                <div class="form-group">
                    <label>{{ trans('sentence.website') }}:</label>
                    <input class="form-control" type="text" name="website" value="{{ $company->website }}" readonly>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection