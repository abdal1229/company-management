@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
            <form action="{{ route('company.store') }}" method="post" enctype="multipart/form-data" class="addEditCompany">
            @csrf
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           {{ trans('sentence.create')." ".trans('sentence.company') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>{{ trans('sentence.name') }}:</label>
                        <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.email') }}:</label>
                        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.logo') }}:</label>
                        <div class="thumb-container">
                        <img src="http://via.placeholder.com/350x350.jpg"  class="img-fluid actualthumb" alt="">
                        <button type="button" class="btn btn-primary  btn-sm btn-icon btn-circle" onclick="showPicker('.thumbnaihiddenpicker')"><i class="flaticon-edit"></i></button>
                        </div>
                        <div class="position-relative">
                        <input type="file" accept="image/png, image/jpeg, image/jpg"  name="logo" class="thumbnaihiddenpicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.website') }}:</label>
                        <input class="form-control" type="text" name="website" value="{{ old('website') }}">
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">{{ trans('sentence.create') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection