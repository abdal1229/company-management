@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Companies
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin::Section-->
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th>#</th>
                                    <th>{{ trans('sentence.logo') }}</th>
                                    <th>{{ trans('sentence.name') }}</th>
                                    <th>{{ trans('sentence.email') }}</th>
                                    <th>{{ trans('sentence.actions') }}</th>
                                </thead>
                                <tbody>
                                    @foreach ($companies as $i => $company)
                                    <tr>
                                        <th>{{ $i + $companies->firstItem() }}</th>
                                        <td>
                                            <div class="user-thumbnail-img"><img src="{{ asset("public/storage/".$company->logo) }}" alt="" class="img-fluid"></div>
                                        </td>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->email }}</td>
                                        <td>
                                            <a href="{{ route('company.show', $company->id) }}" class="btn btn-info btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-eye"></span></a>
                                            <a href="{{ route('company.edit', $company->id) }}" class="btn btn-warning btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-edit"></span></a>
                                            <form action="{{ route('company.destroy', $company->id) }}" method="post" class="d-inline-block">
                                                @csrf @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-delete"></span></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $companies->links() }}

                        </div>
                    </div>
                </div>

                <!--end::Section-->
            </div>

        </div>
    </div>
</div>
@endsection

@section('toolbar')
<a href="{{ route('company.create') }}" class="btn btn-success btn-elevate btn-circle btn-icon" data-toggle="kt-tooltip" data-container="body" data-placement="bottom" title="" data-original-title="{{ trans('sentence.add')." ".trans('sentence.company') }}"><i class="flaticon2-plus"></i></a> 
@endsection