<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Company Management</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Custom Styles(used by this page) -->
		<link href="{{ asset('public/vendor/assets/css/pages/login.css') }}" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{ asset('public/vendor/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('public/vendor/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="{{ asset('public/vendor/assets/css/skins/header/base/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('public/vendor/assets/css/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('public/vendor/assets/css/skins/brand/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('public/vendor/assets/css/skins/aside/dark.css') }}" rel="stylesheet" type="text/css" />

		<!--end::Layout Skins -->
        <link rel="shortcut icon" href="{{ asset('public/vendor/assets/media/logos/favicon.ico') }}" />
        <style>
            .forgot-form.kt-login__title{
margin-bottom: 0 !important;
}
            </style>
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<!-- begin:: Page -->
		<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

					<!--begin::Aside-->
					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{asset('public/vendor/assets/media/bg/bg-4.jpg')}});">
						<div class="kt-grid__item">
                        <a href="{{url('/')}}" class="kt-login__logo">
								<img alt="Logo" style="max-width: 100px;" src="{{ asset('public/img/logo.png') }}" />
							</a>
						</div>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title">Welcome to Company Management!</h3>
							</div>
						</div>
						<div class="kt-grid__item">
							<div class="kt-login__info">
								<div class="kt-login__copyright">
									&copy; 2020 Company Management
								</div>
							</div>
						</div>
					</div>

					<!--begin::Aside-->

					<!--begin::Content-->
					<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
						<!--begin::Body-->
						<div class="kt-login__body">

							<!--begin::Signin-->
							<div class="kt-login__form">
								<div class="forgot-form kt-login__title">
									<h3>Reset Password</h3>
								</div>

								<!--begin::Form-->
                                <form  class="kt-form" method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    
                                    <div class="form-group @error('email') is-invalid @enderror">
                                        <input id="email" placeholder="Email Address" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus required>
                                         @error('email')
                                    <span class="error invalid-feedback" role="alert">
                                       {{ $message }}
                                    </span>
                                     @enderror
                                    </div>         
                                    

                                    <!--begin::Action-->
                                    <div class="kt-login__actions justify-content-around">
                                        <button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Send Password Reset Link</button>
                                    </div>
                                   

                                    <!--end::Action-->
                                </form>

                                <!--end::Form-->

                            </div>

                            <!--end::Signin-->
                        </div>

                        <!--end::Body-->
                    </div>

                    <!--end::Content-->
                </div>
            </div>
        </div>

        <!-- end:: Page -->

		<script src="{{ asset('public/js/init.js') }}" type="text/javascript"></script>

        <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="{{ asset('public/vendor/assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/vendor/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="{{ asset('public/vendor/assets/js/pages/custom/login.js') }}" type="text/javascript"></script>

        <!--end::Page Scripts -->
        @include('layouts.alerts')
    </body>

    <!-- end::Body -->
</html>