<script>
     $(document).ready(function(){

@if($errors->any())             
    toastr.error("There are some errors in your form.<br /> {{ implode('', $errors->all(':message')) }}");
@elseif(session()->has('success'))    
    toastr.success("{{ session()->get('success') }}");
@elseif(session()->has('danger'))   
    toastr.error(" {{ session()->get('danger') }} ");
@elseif(session()->has('status'))   
    toastr.success(" {{ session('status') }} ");
@endif

})
</script>