@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ trans('sentence.dashboard') }}</div>

                <div class="card-body">
                    {{ trans('sentence.welcome-message') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('toolbar')
<form action="#" method="get" class="d-inline-block header-actions">
    <div class="form-group d-flex align-items-center">
        <label>{{ trans('sentence.language') }}:</label>
        <select name="assigned_to" class="form-control d-inline-block w-auto" id="lang-select">
            <option value="">{{ trans('sentence.select')." ".trans('sentence.language') }}</option>
            <option value="{{ route('lang.set-locale', 'en') }}" {{ session('locale') == "en" ? "selected" : "" }} autocomplete="off">English</option>
            <option value="{{ route('lang.set-locale', 'jp') }}" {{ session('locale') == "jp" ? "selected" : "" }} autocomplete="off">よう</option>
        </select>
    </div>
</form>
@endsection
