@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-12">
        <form action="{{ route('employee.update', $employee->id) }}" method="post" enctype="multipart/form-data" class="addEditEmployee">
            @method('PATCH')
            @csrf
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ trans('sentence.edit').' - '.trans('sentence.employee') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>{{ trans('sentence.first').' '.trans('sentence.name') }}:</label>
                        <input class="form-control" type="text" name="first_name" value="{{ old('first_name') ?? $employee->first_name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.last').' '.trans('sentence.name') }}:</label>
                        <input class="form-control" type="last_name" name="last_name" value="{{ old('last_name') ?? $employee->last_name }}">
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.company') }}:</label>
                        <select name="company" class="form-control">
                            <option value="select">Select Company</option>
                            @foreach ($companies as $company)
                        <option value="{{ $company->id }}" {{ $company->id == old('company') ? "selected" : ( $company->id == $employee->company_id ? "selected" : "" ) }}> {{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.email') }}:</label>
                        <input class="form-control" type="text" name="email" value="{{ old('email') ?? $employee->email }}">
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.phone') }}:</label>
                        <input class="form-control" type="text" name="phone" value="{{ old('phone') ?? $employee->phone }}">
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">{{ trans('sentence.update') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection