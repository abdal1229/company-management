@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{ trans('sentence.employees') }}
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--begin::Section-->
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th>#</th>
                                    <th>{{ trans('sentence.first').' '.trans('sentence.name') }}</th>
                                    <th>{{ trans('sentence.last').' '.trans('sentence.name') }}</th>
                                    <th>{{ trans('sentence.company') }}</th>
                                    <th>{{ trans('sentence.email') }}</th>
                                    <th>{{ trans('sentence.phone') }}</th>
                                    <th>{{ trans('sentence.actions') }}</th>
                                </thead>
                                <tbody>
                                    @foreach ($employees as $i => $employee)
                                    <tr>
                                        <th>{{ $i + $employees->firstItem() }}</th>
                                        <td>{{ $employee->first_name }}</td>
                                        <td>{{ $employee->last_name }}</td>
                                        <td><a href="{{ route('company.show', $employee->company->id) }}">{{ $employee->company->name }}</a></td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->phone }}</td>
                                        <td>
                                            <a href="{{ route('employee.show', $employee->id) }}" class="btn btn-info btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-eye"></span></a>
                                            <a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-warning btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-edit"></span></a>
                                            <form action="{{ route('employee.destroy', $employee->id) }}" method="post" class="d-inline-block">
                                                @csrf @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm btn-elevate btn-circle btn-icon"><span class="flaticon-delete"></span></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $employees->links() }}
                        </div>
                    </div>
                </div>

                <!--end::Section-->
            </div>

        </div>
    </div>
</div>
@endsection @section('toolbar')
<a href="{{ route('employee.create') }}" class="btn btn-success btn-elevate btn-circle btn-icon" data-toggle="kt-tooltip" data-container="body" data-placement="bottom" title="" data-original-title="{{ trans('sentence.add').' '.trans('sentence.employee') }}"><i class="flaticon2-plus"></i></a> @endsection