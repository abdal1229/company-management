@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ trans('sentence.view').' - '.trans('sentence.employee') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>{{ trans('sentence.first').' '.trans('sentence.name') }}:</label>
                        <input class="form-control" type="text" name="first_name" value="{{ $employee->first_name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.last').' '.trans('sentence.name') }}:</label>
                        <input class="form-control" type="last_name" name="last_name" value="{{ $employee->last_name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.company') }}:</label>
                        <input class="form-control" type="company" name="company" value="{{ $employee->company->name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.email') }}:</label>
                        <input class="form-control" type="text" name="email" value="{{ $employee->email }}" readonly>
                    </div>
                    <div class="form-group">
                        <label>{{ trans('sentence.phone') }}:</label>
                        <input class="form-control" type="text" name="phone" value="{{ $employee->phone }}" readonly>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection