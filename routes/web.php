<?php

Auth::routes();

Route::middleware('auth')
    ->group(function () {
        Route::get('/', 'DashboardController@index')->name('home.default');
        Route::get('/home', 'DashboardController@index')->name('home');
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('lang/{locale}', 'DashboardController@lang')->name('lang.set-locale');
    });


Route::namespace('Company')
    ->middleware('auth')
    ->group(function () {
        Route::resource('company', 'CompanyController');
    });

Route::namespace('Employee')
    ->middleware('auth')
    ->group(function () {
        Route::resource('employee', 'EmployeeController');
    });
