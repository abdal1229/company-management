## Installation

- create db on your localhost as "company_management"
- clone the repository
- change .env.example to .env
- add mailtrap account detail to check emails
- execute command composer install in power shell or command line