<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Interfaces\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{

    public function all()
    {
        return Employee::paginate(10);
    }

    public function show($id)
    {
        return Employee::findOrFail($id);
    }

    public function create($request)
    {        
        return Employee::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_id' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone
        ]);
    }

    public function update($request, $id)
    {
        $user = Employee::findOrFail($id);
        
        return $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company_id' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone
        ]);
    }

    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        
        return $employee->delete();
    }

}