<?php

namespace App\Repositories;

use App\Models\Company;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function all($paginate=false)
    {
        if($paginate) {
            return Company::paginate(10);
        } else {
            return Company::orderBy('name')->get();
        }
    }

    public function show($id)
    {
        return Company::findOrFail($id);
    }

    public function create($request)
    {
        $logo = null;
        
        if($request->hasFile('logo')) {
            $logo = Storage::disk('public')->put('/', $request->file('logo'));
        }
        
        return Company::create([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website
        ]);
    }

    public function update($request, $id)
    {
        $company = Company::findOrFail($id);
        $logo = $company->logo;

        if($request->hasFile('image')) {
            $logo = Storage::disk('public')->put('/', $request->file('logo'));
            Storage::delete($company->logo);
        }

        $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $logo,
            'website' => $request->website
        ]);

        return $company;
    }

    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        Storage::delete($company->logo);

        return $company->delete();
    }
}