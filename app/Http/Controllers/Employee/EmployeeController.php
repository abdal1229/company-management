<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Interfaces\EmployeeRepositoryInterface;
use App\Interfaces\CompanyRepositoryInterface;

class EmployeeController extends Controller
{
    public function __construct(EmployeeRepositoryInterface $employee, CompanyRepositoryInterface $company)
    {
        $this->employee = $employee;
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => trans('sentence.employee').' - '.trans('sentence.listing'),
            'employees' => $this->employee->all()
        ];

        return view('employee.listing', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => trans('sentence.employee').' - '.trans('sentence.create'),
            'companies' => $this->company->all(),
        ];

        return view('employee.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Backend\EmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $this->employee->create($request);

        return redirect()->route('employee.index')->with('success', 'Employee created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'title' => trans('sentence.employee').' - '.trans('sentence.view'),
            'employee' => $this->employee->show($id)
        ];

        return view('employee.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => trans('sentence.employee').' - '.trans('sentence.edit'),
            'companies' => $this->company->all(),
            'employee' => $this->employee->show($id),
        ];

        return view('employee.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Backend\EmployeeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $this->employee->update($request, $id);

        return redirect()->back()->with('success', 'Employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->employee->destroy($id);

        return redirect()->back()->with('success', 'Employee deleted successfully');
    }
}
