<?php

namespace App\Http\Controllers;

use App;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [
            'title' => trans('sentence.dashboard')
        ];

        return view('dashboard', $data);
    }

    public function lang($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }
}
