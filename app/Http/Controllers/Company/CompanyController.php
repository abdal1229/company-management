<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Interfaces\CompanyRepositoryInterface;
use App\Http\Requests\CompanyRequest;

class CompanyController extends Controller
{
    public function __construct(CompanyRepositoryInterface $company)
    {
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => trans('sentence.company').' - '.trans('sentence.listing'),
            'companies' => $this->company->all(true)
        ];

        return view('company.listing', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'title' => trans('sentence.company').' - '.trans('sentence.view'),
            'company' => $this->company->show($id)
        ];

        return view('company.view', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => trans('sentence.company').' - '.trans('sentence.create'),
        ];

        return view('company.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Backend\CompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $this->company->create($request);

        return redirect()->route('company.index')->with('success', 'Company created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => trans('sentence.company').' - '.trans('sentence.edit'),
            'company' => $this->company->show($id),
        ];

        return view('company.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Backend\CompanyRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $this->company->update($request, $id);

        return redirect()->route('company.index')->with('success', 'Company updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->company->destroy($id);

        return redirect()->back()->with('success', 'Company deleted successfully');
    }
}
