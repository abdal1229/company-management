<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getName()) {
            case 'employee.store':
                return [
                    'first_name' => 'required|alpha_spaces|max:255',
                    'last_name' => 'required|alpha_spaces|max:255',
                    'company' => 'required|exists:companies,id',
                    'email' => 'required|unique:employees,email',
                    'phone' => 'integer',
                ];
            break;
            case 'employee.update':
                return [
                    'first_name' => 'required|alpha_spaces|max:255',
                    'last_name' => 'required|alpha_spaces|max:255',
                    'company' => 'required|exists:companies,id',
                    'email' => 'required|unique:employees,email,'.$this->route('employee'),
                    'phone' => 'required|integer',
                ];
            break;
        }
    }
}
