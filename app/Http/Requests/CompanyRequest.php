<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getName()) {
            case 'company.store':
                return [
                    'name' => 'required|alpha_spaces',
                    'email' => 'required|email|unique:companies,email',
                    'logo' => 'image',
                    'website' => 'url'
                ];
            break;
            case 'company.update':
                return [
                    'name' => 'required|alpha_spaces',
                    'email' => 'required|email|unique:companies,email,'.$this->route('company'),
                    'logo' => 'image',
                    'website' => 'url'
               ];
            break;
        }
    }
}
