<?php

namespace App\Interfaces;

interface EmployeeRepositoryInterface
{
    public function all();

    public function show($id);

    public function create($request);

    public function update($request, $id);

    public function destroy($id);
}