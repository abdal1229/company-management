<?php

namespace App\Interfaces;

interface CompanyRepositoryInterface
{
    public function all($paginate=false);

    public function show($id);

    public function create($request);

    public function update($request, $id);

    public function destroy($id);
}