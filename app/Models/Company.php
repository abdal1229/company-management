<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $guarded = [
        'id'
    ];

    public function user()
    {
        return $this->hasMany(Employee::class);
    }
}
