// Class definition

var KTBootstrapDatetimepicker = function() {

    // Private functions
    var demos = function() {
        // minimal setup
        $('.companyDates').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            format: 'yyyy-mm-dd hh:ii:ss'
        });
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
    KTBootstrapDatetimepicker.init();
});