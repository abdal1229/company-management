$(".thumbnaihiddenpicker").change(function() {
    if (this.files && this.files[0]) {
        if (this.files[0].type == 'image/png' || this.files[0].type == 'image/jpeg' || this.files[0].type == 'image/png') {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.actualthumb').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    }
});

function preloader(action) {
    if (action == 'show') {
        $('.preloader').fadeIn(300)
    } else {
        $('.preloader').hide();
    }
}

function showPicker(elemName) {
    $(elemName).click();
}

var addEditCompanyValidator = $('.addEditCompany').validate({
    rules: {
        name: {
            required: true,
            minlength: 1,
            maxlength: 30,
            lettersAndSpacesOnly: true
        },
        email: {
            email: true,
            required: true
        },
        logo: {
            accept: "image/png, image/jpeg, image/jpg"
        },
        website: {
            url: true
        }
    },
    messages: {
        logo: {
            accept: "Please upload a valid image"
        }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
        $(error).addClass('invalid-feedback').insertAfter(element);
    },
    errorClass: 'is-invalid',
    validClass: 'is-valid',
    submitHandler: function(form) {
        preloader('show');
        form.submit();
    }
});

var addEditEmployeeValidator = $('.addEditEmployee').validate({
    rules: {
        first_name: {
            required: true,
            minlength: 1,
            maxlength: 255,
            lettersAndSpacesOnly: true
        },
        last_name: {
            required: true,
            minlength: 1,
            maxlength: 255,
            lettersAndSpacesOnly: true
        },
        company: {
            required: true
        },
        email: {
            email: true,
            required: true
        },
        phone: {
            onlyInteger: true
        }
    },
    messages: {
        logo: {
            accept: "Please upload a valid image"
        }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
        $(error).addClass('invalid-feedback').insertAfter(element);
    },
    errorClass: 'is-invalid',
    validClass: 'is-valid',
    submitHandler: function(form) {
        preloader('show');
        form.submit();
    }
});

$('#fakeFilePicker').on('focus', function() {
    $('#realFilePicker').click();
    $('#fakeFilePicker').blur();
})


toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "10000",
    "extendedTimeOut": "2000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

$('.showTooltips').tooltip();

$(document).ready(function() {
    $('#lang-select').on('change', function(e) {
        var form = $(this).closest('form');
        var action = $(this).val();
        if (action) {
            form.attr('action', action);
            form.submit();
        }
    });
})